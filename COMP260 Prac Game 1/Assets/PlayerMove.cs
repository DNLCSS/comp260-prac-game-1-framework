﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {	
	
	public Vector2 move;
	public Vector2 velocity;
	public int player;
	
	//public GameObject player1;
	//public GameObject player2;

	public float maxSpeed = 5.0f;
	public float acceleration = 6.0f;
	public float brake = 8.0f;
	public float turnSpeed = 30.0f;
	private float speed = 0.0f;
	
	private BeeSpawner beeSpawner;
	public float destroyRadius = 1.0f;

	// Use this for initialization
	void Start () {
		// oh shit where the bee spawner at
		beeSpawner = FindObjectOfType<BeeSpawner>();
	}	
	
	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal"+player as string);
		direction.y = Input.GetAxis("Vertical"+player as string);
		float turn = Input.GetAxis("Horizontal"+player as string);
		float forwards = Input.GetAxis("Vertical"+player as string);
		
		if(forwards > 0) {
			speed = speed + acceleration * Time.deltaTime;
		} else if(forwards < 0) {
			speed = speed - acceleration * Time.deltaTime;
		} else {
			// braking
			if(speed > 0) {
				speed = Mathf.Clamp(speed - brake * Time.deltaTime, 0, maxSpeed);
			} else {
				speed = Mathf.Clamp(speed + brake * Time.deltaTime, -maxSpeed, 0);
			}
		}
		
		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
		// compute a vector in the up direction of length speed;
		Vector2 velocity = Vector2.up * speed;
		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
		// rotate the object
		transform.Rotate(0, 0, -turn * turnSpeed * Time.deltaTime * speed);
	
		if(Input.GetButtonDown("Fire1")) {
			// too many bees in my life
			beeSpawner.DestroyBees(transform.position, destroyRadius);
		}	
	}
	
	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, destroyRadius);
	}
}
