﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {	
	
	public float minSpeed, maxSpeed, minTurnSpeed, maxTurnSpeed;
	private float speed, turnSpeed;
	
	private Transform target;	
	private Vector2 heading;
	
	public ParticleSystem explosionPrefab;
	// public Transform target2;
	// public Transform target3;
	// public Transform target4;	
	
	// private Vector2 heading = Vector2.right;

	// Use this for initialization
	void Start () {
		// find player
		List<PlayerMove> totalPlayers = FindObjectsOfType<PlayerMove>();
		
		PlayerMove p = totalPlayers[0];


		target = p.transform;
		
		
		// bee free
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);
		
		// bee fast
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
		
	}
	
	// Update is called once per frame
	void Update () {
		// get the vector from the bee to the target and normalise it
		Vector2 d1 = target.position - transform.position;
		// Vector2 d2 = target2.position - transform.position;
		// Vector2 d3 = target3.position - transform.position;
		// Vector2 d4 = target4.position - transform.position;
				
		// Vector2 direction = Vector2.Min(Vector2.Min(d1, d2), Vector2.Min(d3, d4));
		Vector2 direction = d1;
		
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;
		// turn left or right
		if (direction.IsOnLeft(heading)) {
			//target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		} else {
			//target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
		transform.Translate(heading * speed * Time.deltaTime);
	}
	
	void OnDestroy() {
		// RIP in pieces, bees
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// RIP in pieces, binary numbers that vaguely resemble explosions
		Destroy(explosion.gameObject, explosion.main.duration);
	}
	
	// void OnDrawGizmos() {
		// //draw heading vector
		// Gizmos.color = Color.red;
		// Gizmos.DrawRay(transform.position, heading);
		
		// //draw target vector
		// Gizmos.color = Color.yellow;
		
		// Vector2 direction = target.position - transform.position;
		// Gizmos.DrawRay(transform.position, direction);
		
		// Vector2 direction2 = target2.position - transform.position;
		// Gizmos.DrawRay(transform.position, direction2);
		
		// Vector2 direction3 = target3.position - transform.position;
		// Gizmos.DrawRay(transform.position, direction3);
		
		// Vector2 direction4 = target4.position - transform.position;
		// Gizmos.DrawRay(transform.position, direction4);
	// }
}
