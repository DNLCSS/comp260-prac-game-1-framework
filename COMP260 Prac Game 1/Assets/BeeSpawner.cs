﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
	
	public int nBees = 50;
	public BeeMove beePrefab;
	public Rect spawnRect;
	
	private float beePeriod = 1.0f;
    private float timer = 0.0f;
	
	public float minBeePeriod = 1.0f;
	public float maxBeePeriod = 5.0f;
	
	private int randomBee;
	
	// Use this for initialization
	void Start () {
		// set up counter for random bees
		randomBee = nBees;
		
		// Oh god not the bees
		for(int i=0; i<nBees; i++){
			// Oh fuck there's so many
			BeeMove bee = Instantiate(beePrefab);
			// Better make sure they know who daddy is
			bee.transform.parent = transform;
			// Aw they have names too
			bee.gameObject.name = "Bee " + i;
			
			// move the bee to a random position within the spawn rectangle
			float x = spawnRect.xMin + Random.value * spawnRect.width;
			float y = spawnRect.yMin + Random.value * spawnRect.height;
			bee.transform.position = new Vector2(x,y);
		}
	}
	
	// Update is called once per frame
    void Update() {
        timer += Time.deltaTime;
        
		// check if allotted time has passed
		if (timer > beePeriod) {            
			// Hello bees
			BeeMove bee = Instantiate(beePrefab);
			// Better make sure they know who daddy is
			bee.transform.parent = transform;
			// Aw they have names too
			bee.gameObject.name = "Bee " + randomBee;
			// increase counter
			randomBee++;
			
			// move the bee to a random position within the spawn rectangle
			float x = spawnRect.xMin + Random.value * spawnRect.width;
			float y = spawnRect.yMin + Random.value * spawnRect.height;
			bee.transform.position = new Vector2(x,y);
			
            // Remove the recorded 2 seconds.
            timer = timer - beePeriod;
			
			// generate new random allotted time
			beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
        }
	}
	
	public void DestroyBees(Vector2 centre, float radius) {
		// Do you feel there are too many bees in your life?
		for(int i=0; i<transform.childCount; i++) {
			Transform child = transform.GetChild(i);			
			Vector2 v = (Vector2)child.position - centre;
			if(v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}		
	}
	
	void OnDrawGizmos() {
		//draw the spawning rectangle
		Gizmos.color = Color.green;
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMin),
			new Vector2(spawnRect.xMax, spawnRect.yMin));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMin),
			new Vector2(spawnRect.xMax, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMax),
			new Vector2(spawnRect.xMin, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMax),
			new Vector2(spawnRect.xMin, spawnRect.yMin));
	}
}
